# dwm - dynamic window manager
# See LICENSE file for copyright and license details.

include config.mk

SRC = src/drw_text.c src/main.c src/util.c src/apply.c src/arrange-attach.c src/button.c src/clean.c src/client.c src/misc.c src/config.c src/monitor.c src/notify.c src/bar.c src/layout.c src/mouse.c src/focus.c src/get.c src/key.c src/manage.c src/xorg.c src/core.c src/update.c src/set.c src/view.c src/utf.c src/font.c src/drw_core.c
OBJ = bin/drw_text.o bin/main.o bin/util.o bin/apply.o bin/arrange-attach.o bin/button.o bin/clean.o bin/client.o bin/misc.o bin/config.o bin/monitor.o bin/notify.o bin/bar.o bin/layout.o bin/mouse.o bin/focus.o bin/get.o bin/key.o bin/manage.o bin/xorg.o bin/core.o bin/update.o bin/set.o bin/view.o bin/utf.o bin/font.o bin/drw_core.o

all: neodwm

bin/%.o: src/%.c
	${CC} -c ${CFLAGS} -o $@ $<

${OBJ}: config.h config.mk

neodwm: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean::
	rm -f neodwm ${OBJ} neodwm-${VERSION}.tar.gz

dist: clean
	mkdir -p neodwm-${VERSION}
	cp -R LICENSE Makefile README config.def.h config.mk\
		misc/neodwm.1 include/drw.h include/util.h ${SRC} neodwm.png neodwm-${VERSION}
	tar -cf neodwm-${VERSION}.tar neodwm-${VERSION}
	gzip neodwm-${VERSION}.tar
	rm -rf config.h neodwm-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f neodwm ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/neodwm
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < misc/neodwm.1 > ${DESTDIR}${MANPREFIX}/man1/neodwm.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/neodwm.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/dwm\
		${DESTDIR}${MANPREFIX}/man1/dwm.1

.PHONY: all clean dist install uninstall
